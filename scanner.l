%{ 
    #include <stdio.h>
    #include "parser.tab.h"      // obtained from yacc file
    #include<stdlib.h>
    int i;
    #define border printf("\n"); for(i=0; i<=80; ++i) { putchar('-'); } printf("\n");
    int yyerror(char *errorMsg);
    int yyparse (void);
    void success(char *successMsg);
%}

%%
NEWLINE;
INDENT;
DEDENT; 
"print" return (PRINT);
"for" return (FOR);
"in" return (IN);
"and" return (AND);
"or" return (OR);
"not" return (NOT);
'<' return (LESS); 
'>' return (GREATER);
'<=' return (LESSOREQ); 
'>=' return (GREATEROREQ);
'==' return (EQUAL);
"range" return (RANGE);
"xrange" return (XRANGE);
"if" return (IF);
"and"|"assert"|"break"|"class"|"def"|"del"|"elif" return (KEYWORD);
"else"|"except"|"exec"|"finally"|"for"|"from"|"global" return (KEYWORD);
"if"|"import"|"in"|"is"|"lambda"|"not"|"or"|"pass"|"raise" return (KEYWORD);
"return"|"try"|"while"|"with"|"yield" return (KEYWORD);

[0-9]+                  return NUMBER;
[_a-zA-Z][_a-zA-Z0-9]*  return ID ;

[\t]		return TAB;		
[\n]        return NL;		
\:          return (COLON);
" "			;
["#"][0-9A-Za-z#$%=@!{},`~&*()<>?.:;_|^/+\t\r\n\[\]'-]+[\n]	;
\=                      return (EQ);
\+                      return (PLUS);
\-                      return (MINUS);
\*                      return (MUL);
\/                      return (DIVIDE);
\(                      return (LBRACKET);
\)                      return (RBRACKET);
\"([^\"\n])*\"          return (STRING_LIT);
\'([^\'\n])*\'          return (STRING_VAR);  
'.' return (NAME);
%%

int yywrap(){}

