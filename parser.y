%{
    #include <stdio.h>
    int yydebug=1;
    int yylex(void);
    #define border printf("\n"); for(i=0; i<=80; ++i) { putchar('-'); } printf("\n");
    int i;
    int yyerror(char *errorMsg);
    void success(char *successMsg);
%}

// tokens
%token ID NUMBER STRING_LIT STRING_VAR NEWLINE INDENT DEDENT NAME
%token EQ PLUS MINUS MUL DIVIDE LBRACKET RBRACKET TAB NL AND OR NOT
%token COLON FOR IN RANGE XRANGE COMMA GREATER LESS GREATEROREQ LESSOREQ EQUAL
%token PRINT KEYWORD IF

// set precedence
%right EQ
%left PLUS MINUS
%left MUL DIVIDE
%left AND
%left OR
%%

/* Parser Grammar */

input:
|line input 

line: 
statements NL
|NL ;

statements:
      assign_arithmetic 
    | assign_str 
    | return_stmt
    | 'break' 
    | 'continue' 
    | display
    | function_def
    | if_stmt 
    | for_stmt
    | while_stmt
    ;


assign_arithmetic: identifier EQ expr 
    ;

assign_str:  identifier EQ strings 
    ;

strings: STRING_LIT | STRING_VAR
    ;

expr: expr PLUS expr
    | expr MINUS expr
    | expr MUL expr
    | expr DIVIDE expr
    | factor
    | LBRACKET expr RBRACKET
    | SIGN factor
    ;

comparsion: 
      expr AND expr
    | expr OR expr
    | NOT expr
    | LBRACKET comparsion RBRACKET
    | expr GREATEROREQ expr
    | expr GREATER expr
    | expr LESS expr
    | expr LESSOREQ expr
    | expr EQUAL expr 
    ;

SIGN: PLUS
    | MINUS
    ;

factor: identifier 
    |   NUMBER
    ;

return_stmt:
    | 'return' identifier
    | 'return' comparsion
    | 'return' NUMBER
    | 'return' STRING_LIT
    | 'return' STRING_VAR 
    ;

block:
    |NEWLINE INDENT statements DEDENT 
    ;
    
function_def:
    | 'def' NAME '('params')' ':' block 
    ;

params: ID ','
        | params
        ; 

if_stmt:
    IF comparsion ':'
    |IF comparsion ':' block elif_stmt 
    | IF comparsion ':' block else_block
    ;
elif_stmt:
    | 'elif' comparsion ':' block elif_stmt 
    | 'elif' comparsion ':' block else_block
    ;
else_block:
    | 'else' ':' block 
    ;

while_stmt:
    | 'while' comparsion ':' block
    ;


for_stmt:
    | 'for' identifier 'in' times ':' block  
    ;

times: identifier | range | xrange

range: RANGE LBRACKET myrange RBRACKET
    | RANGE LBRACKET myfunc RBRACKET

myfunc: identifier LBRACKET RBRACKET

myrange : NUMBER 
        | NUMBER COMMA NUMBER
        | NUMBER COMMA NUMBER COMMA NUMBER
        
        
xrange: XRANGE LBRACKET myrange RBRACKET

identifier: ID | keyword 
    ;
        
keyword: PRINT | KEYWORD 
		| FOR
        | IN
        | RANGE
        | XRANGE
    ;
    

display: PRINT strings
    | PRINT strings MUL NUMBER
    | PRINT strings PLUS strings
    | PRINT expr
    ;

%%
int main() {
    printf("\n--------------------------- PYTHON EXPRESSION PARSER ---------------------------\n");
    printf("\nEnter a python expression : \n");
    yyparse();
    return 0;
}

int yyerror(char* a){
  printf(" ERROR: %s\n",a);
  border;
  return 0;
}

void success(char *successMsg) {
    fprintf(stderr, "\n%s\n", successMsg);
    printf("\n");
    border;
}


